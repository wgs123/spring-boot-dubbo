package org.spring.springboot.dubbo.impl;

import org.springframework.stereotype.Component;

import com.alibaba.dubbo.config.annotation.Reference;
@Component
public class Serviceimp {
	@Reference(version = "1.0.0")
    org.spring.springboot.dubbo.Service service;
    
    public void test(){
    	service.test();
    }
}
