package org.spring.springboot.controller;

import java.io.UnsupportedEncodingException;

import org.spring.springboot.dubbo.CityDubboConsumerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.dubbo.common.json.JSONObject;
@Controller
public class TestController {
	@Autowired
	CityDubboConsumerService cityDubboConsumerService;
	@RequestMapping("/test")
	@ResponseBody
	public byte[] test() throws UnsupportedEncodingException{
		JSONObject json=new JSONObject();
		cityDubboConsumerService.printCity();
		json.put("result", true);
		return json.toString().getBytes("UTF-8");
	}
}
